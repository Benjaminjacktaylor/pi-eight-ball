#pragma once

class GPIOPin
{
public:
	GPIOPin(std::string inputGPIONumber);

	~GPIOPin();

	int exportGPIO();

	int unexportGPIO();

	//in or out
	int setDirection(std::string direction);

	int setValue(std::string value);

	int getValue(std::string& value);

	std::string getGPIONumber();

private:
	std::string GPIONumber;
};
