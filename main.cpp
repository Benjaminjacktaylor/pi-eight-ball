#include <iostream>
#include "GPIOPin.h"

int main()
{
	std::cout << "Started pi-eight-ball ";
	std::cout << "V0.0.10\n";

	GPIOPin pin = GPIOPin("17");

	pin.exportGPIO();
	pin.setDirection("in");

	bool buttonPressed = false;
	int clickCount = 0;
	bool quit = false;

	while (!quit)
	{
		std::string value;
		pin.getValue(value);
		if (buttonPressed) //pressed
		{
			if (value == "0")
			{
				std::cout << "Click ended\n";

				buttonPressed = false;
			}
		} else //not pressed
		{
			if (value == "1")
			{
				buttonPressed = true;
				std::cout << "Click " << clickCount << std::endl;
				clickCount++;
				if (clickCount == 20)
				{
					quit = true;
				}
			}
		}


		//LOGIC 2
		//if (value == "0")
		//{
		//	if (buttonPressed)
		//	{
		//		std::cout << "Click ended\n";
		//	}
		//
		//	buttonPressed = false;
		//} else //1
		//{
		//	if (!buttonPressed)
		//	{
		//		buttonPressed = true;
		//		std::cout << "Click " << clickCount << std::endl;
		//		clickCount++;
		//		if (clickCount == 20)
		//		{
		//			quit = true;
		//		}
		//	}
		//}
	}

	pin.unexportGPIO();

	std::cout << "pi-eight-ball ended\n";

	return 0;
}
