#include <iostream>
#include <fstream>
#include <sstream>
#include "GPIOPin.h"

GPIOPin::GPIOPin(std::string inputGPIONumber)
{
	GPIONumber = inputGPIONumber;
}

GPIOPin::~GPIOPin()
{

}

int GPIOPin::exportGPIO()
{
	std::string exportString = "/sys/class/gpio/export";
	std::ofstream exportGPIO(exportString.c_str()); // Open "export" file. Convert C++ string to C string. Required for all Linux pathnames
	//	if (exportGPIO < 0)
	//	{
	//		std::cout << " OPERATION FAILED: Unable to export GPIO" << GPIONumber << " .\n";
	//		return -1;
	//	}

	exportGPIO << GPIONumber; //write GPIO number to export
	exportGPIO.close(); //close export file
	return 0;
}

int GPIOPin::unexportGPIO()
{
	std::string unexportString = "/sys/class/gpio/unexport";
	std::ofstream unexportGPIO(unexportString.c_str()); //Open unexport file
	//	if (unexportGPIO < 0)
	//	{
	//		std::cout << " OPERATION FAILED: Unable to unexport GPIO" << GPIONumber << " .\n";
	//		return -1;
	//	}

	unexportGPIO << GPIONumber; //write GPIO number to unexport
	unexportGPIO.close(); //close unexport file
	return 0;
}

int GPIOPin::setDirection(std::string direction)
{
	std::string setDirectionString = "/sys/class/gpio/gpio" + GPIONumber + "/direction";
	std::ofstream setDirectionGPIO(setDirectionString.c_str()); // open direction file for gpio
	//	if (setDirectionGPIO < 0)
	//	{
	//		std::cout << " OPERATION FAILED: Unable to set direction of GPIO" << GPIONumber << " .\n";
	//		return -1;
	//	}

	setDirectionGPIO << direction; //write direction to direction file
	setDirectionGPIO.close(); // close direction file
	return 0;
}

int GPIOPin::setValue(std::string value)
{
	std::string setValueString = "/sys/class/gpio/gpio" + GPIONumber + "/value";
	std::ofstream setValueGPIO(setValueString.c_str()); // open value file for gpio
	//	if (setValueGPIO < 0)
	//	{
	//		std::cout << " OPERATION FAILED: Unable to set the value of GPIO" << GPIONumber << " .\n";
	//		return -1;
	//	}

	setValueGPIO << value;//write value to value file
	setValueGPIO.close();// close value file
	return 0;
}

int GPIOPin::getValue(std::string& value)
{
	std::string getValueString = "/sys/class/gpio/gpio" + GPIONumber + "/value";
	std::ifstream getValueGPIO(getValueString.c_str());// open value file for gpio
	//	if (getValueGPIO < 0)
	//	{
	//		std::cout << " OPERATION FAILED: Unable to get value of GPIO" << GPIONumber << " .\n";
	//		return -1;
	//	}

	getValueGPIO >> value;  //read gpio value

	if (value != "0")
		value = "1";
	else
		value = "0";

	getValueGPIO.close(); //close the value file
	return 0;
}

std::string GPIOPin::getGPIONumber()
{
	return GPIONumber;
}
