cmake_minimum_required(VERSION 3.7.2)
project(pi_eight_ball)

set(CMAKE_CXX_STANDARD 11)

add_executable(pi_eight_ball
        main.cpp GPIOPin.cpp GPIOPin.h)
